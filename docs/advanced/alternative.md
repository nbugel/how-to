# Deploy a `mkdocs` site on paas.cern.ch

If the [new infra](/gitlabpagessite/create_site/) based on GitLab Pages doesn't meet your requirements you can still deploy your MkDocs site on OKD4 (paas.cern.ch) using our custom [MkDocs configuration](https://gitlab.cern.ch/authoring/documentation/mkdocs-helm-example).


!!! warning
    If you are migrating from `openshift.cern.ch` to PaaS and you want to keep the old URL, then you need to make sure that the corresponding project is deleted from `webservices.web.cern.ch` (this only applies to sites that used `.web.cern.ch` instead of `.docs.cern.ch`). So, before you start the process described below, delete the project and wait for the email confirming the deletion. Only after that can you proceed with the migration.

## Create a project on [paas.cern.ch](https://paas.cern.ch). Make sure that you are in the `Administrator` view (not `Developer`).

![](/images/alternative-deployment/step-1-1.png)

Then click `Create Project` in the top right corner. Fill in the form and click `Create`.

![](/images/alternative-deployment/step-1-2.png)

## Create a Service Account

!!! info
    Before you proceed, make sure you are in the `Administrator` view of your project.

Now navigate to `User Management` -> `ServiceAccounts` and create a new `ServiceAccount` (click `Create ServiceAccount`).

![](/images/alternative-deployment/step-2-1.png)

Choose a name for the `ServiceAccount` (e.g. `gitlab-deployer`) and click `Create`.

![](/images/alternative-deployment/step-2-2.png)

## Create a RoleBinding

Now we need to create a `RoleBinding` for the `ServiceAccount` created in the previous step. Go to `User Management` -> `RoleBindings` and click `Create binding`.

![](/images/alternative-deployment/step-3-1.png)

Now it is time to correctly bind `edit` `Role` to the `ServiceAccount` within your project. Some remarks

* **Name of the `RoleBinding`** - it is not really important therefore you can choose whatever name you like (in this case I used the same name that I used for the `ServiceAccount`)
* **Namespace of the `RoleBinding`** - the name of the project we created in one of the previous steps
* **Role** - `edit`
* **Subject** - `ServiceAccount`
* **Subject namespace** - the name of the project we created in one of the previous steps
* **Subject name** - the name of the `ServiceAccount` we created in the previous step

![](/images/alternative-deployment/step-3-2.png)


Click `Create` to create the `RoleBinding`.

## Get ServiceAccount's token

Now we need to copy the `ServiceAccount`'s token that we will use in the next steps. Go to `User Management` -> `ServiceAccounts` and click on the name of the `ServiceAccount` you created in one of the previous steps.

![](/images/alternative-deployment/step-4-1.png)

On the bottom you will see section `Secrets`. Locate the secret of type `kubernetes.io/service-account-token` and click its name. You will see the `Secret` details. We are now interested in copying the value of the `token` key. Locate the `token` row inside the `Data` section and click the icon on the right side. Save the copied value somewhere since we will need it soon.

![](/images/alternative-deployment/step-4-2.png)

## Store secret and project name in CI/CD variables

Now we need to configure `GitLab CI/CD` to automate the deployment of changes to production. Go to [GitLab](https://gitlab.cern.ch) and navigate to the repository with your documentation site. Now, navigate to `Settings` -> `CI/CD`.

![](/images/alternative-deployment/step-5-1.png)

Expand `Variables` section and click `Add variable`. Fill in the form with the following values:

* **Key** - `OKD_TOKEN`
* **Value** - use the value of the `token` we copied in the previous step
* **Mask variable** - make sure this option is enabled


## Add extra configuration to your GitLab project

* First, we need to add a special directory that contains the custom `Helm` chart that we have created. This chart includes definitions of components required to deploy a `MkDocs` site onto [paas.cern.ch](https://paas.cern.ch). Go to [mkdocs-helm-example](https://gitlab.cern.ch/authoring/documentation/mkdocs-helm-example) and copy the entire `chart` directory into your repository with the following modifications:
    * according to your needs you will need to change the `chart/values-override.yaml` file (more information is provided in the project's `README.md`)
    * make sure that you at least specify `route.host` value (the URL of your website)
    * by default your site will be only accessible from within the CERN network unless you set `route.public` to `true`
* You should now copy the contents of the `.gitlab-ci.yml` from the [mkdocs-helm-example](https://gitlab.cern.ch/authoring/documentation/mkdocs-helm-example) into your repository's `.gitlab-ci.yml`. While doing that remember to set `variables.PROJECT` to the name of the project that you created in one of the first steps.

Once you commit all these changes, your site should be built and deployed automatically onto [paas.cern.ch](https://paas.cern.ch)

## SSO-protected site

By default, if you don't change the default values of `route.create` and `route.public` your site will be only accessible from within the CERN network without any authentication. In order to make it SSO-protected you will need to [deploy a special proxy](https://paas.docs.cern.ch/4._CERN_Authentication/2-deploy-sso-proxy/). In order to do that you need to perform the following steps:

* Modify your `values-override.yaml` and make sure that `route.create` is set to `false` and `route.host` equals to an empty string. Keep in mind that this will make route site inaccessible until you properly deploy the SSO proxy

```yaml
route:
  create: false
  host: ''
  public: false
```

* Wait until the pipeline succeeds and the `Route` is deleted from the project
* Now go to the project on [paas.cern.ch](https://paas.cern.ch) which hosts your MkDocs deployment
* Select your project from the list and switch to the `Developer` view

![](/images/alternative-deployment/step-7-1.png)

* Now click `+Add`

![](/images/alternative-deployment/step-7-2.png)

* And now select `Helm Chart`

![](/images/alternative-deployment/step-7-3.png)

* Now search for `Cern Auth Proxy` and select it once you find it

![](/images/alternative-deployment/step-7-4.png)

* Once you select it, an overlay will show up one the right side of the screen. Click `Install Helm Chart`

![](/images/alternative-deployment/step-7-5.png)

* Now you will need to provide necessary values to deploy SSO proxy in your project.
    * **Release Name** - this can be anything
    * **Upstream Application**
        * **Service definition**
            * **Service name** - usually `mkdocs` unless you have changed GitLab CI variable `RELEASE_NAME`. If you did, you can find the name of the `Service` under `Networking` -> `Services` in the administrator view
            * **Service port** - 8080
    * **Routing Configuration**
        * **Public Application Hostname** - it is the URL of your MkDocs site
        * **Application Subpath** - leave the default `/`.
        * **Internet Visibility** - leave ticket if you want your site to be available outside of the CERN network
    * Then click `Install`


For more information on how to customize the SSO part, you can refer to the [official FAQ](https://paas.docs.cern.ch/4._CERN_Authentication/2-deploy-sso-proxy/#faq).

## Remove old resources

You can refer to the [docs](/gitlabpagessite/migration/keep_it_clean/) to find out what should be deleted once you migrate your site from Openshift 3.
