# Use locally hosted Google Fonts

To respect user privacy and to avoid disclosure of users data, we must avoid downloading Google Fonts from their servers. The easiest way of resolving this problem is to configure your `mkdocs` site to use locally hosted fonts. If you create a `mkdocs` site from our [template](/gitlabpagessite/create_site/creategitlabrepo/create_with_mkdocs/#a-import-project) (as of July 11th 2023) you won't need to perform the steps below as this template is already configured to use locally hosted fonts.

1. Download the necessary files and put them under `docs/`

    ```bash
    cd <directory-with-your-documentation>
    curl -fsSL --output fonts.zip "https://gitlab.cern.ch/authoring/documentation/mkdocs-container-example/-/archive/master/mkdocs-container-example-master.zip?path=docs/stylesheets"
    unzip -d fonts fonts.zip
    cp -r fonts/mkdocs-container-example-master-docs-stylesheets/docs/stylesheets docs/
    rm -r fonts.zip fonts
    ```

2. Modify your `mkdocs.yml` to include the following configuration:

    ```yaml
    # Remember that this configuration must be merged with the existing configuration (if any)
    theme:
      name: material
      font: false
    extra_css:
      - stylesheets/fonts.css
    ```

3. Commit and push to your GitLab repository. A new job will be created which will rebuild your documentation.
