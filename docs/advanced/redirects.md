# Custom redirects (former custom Nginx)


In the old infrastructure, users could provide a custom Nginx configuration. Now, this configuration can be replaced with a special `_redirects` file.

The `_redirects` file is **expected to be in the root folder** of the repository (it is copied to the `public` folder during `pages` CI job; that folder is later uploaded to S3 from which it is served to the users).

For more information about the format of `_redirects` file, you can refer to the [official documentation](https://gitlab.cern.ch/help/user/project/pages/redirects.md){target=_blank}.

For an old Nginx configuration like this:

```
# 301 permanent redirect for file.html page from the old location,
# under /old_path/ path, to the new location, under /new_path/ path
location /old_path/file.html {
  return 301 /new_path/file.html
}

# 302 temporary redirect for another_file.html page from the old location,
# under /old_path/ path, to the new location, under /new_path/ path
location = /old_path/another_file.html {
  return 302 /another_file.html;
}
```


The equivalent content of `_redirects` file should be:

```
# 301 permanent redirect for file.html page from the old location,
# under /old_path/ path, to the new location, under /new_path/ path
/old_path/file.html /new_path/file.html 301

# 302 temporary redirect for another_file.html page from the old location,
# under /old_path/ path, to the new location, under /new_path/ path
/old_path/another_file.html /new_path/another_file.html 302
```

!!! info
    In order to make sure that all rules are correct, you can access special URL `https://<domain>/_redirects`. More information on how to debug redirect rules can be found [here](https://gitlab.cern.ch/help/user/project/pages/redirects.md#debug-redirect-rules).
