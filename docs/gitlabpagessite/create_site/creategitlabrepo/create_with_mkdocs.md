# Option B. Create a site using MkDocs (Markdown)

If you prefer to use the MkDocs with Markdown sources for your documentation rather than a GitLab template, you should follow this [structure](https://www.mkdocs.org/user-guide/writing-your-docs/){target=_blank}.

Depending on your experience and/or how brave you are, you can choose **one** of the following options to start developing your documentation:

**A.** [Import project](create_with_mkdocs.md#a-import-project): If you want to start with an example MkDocs structure, continue by importing an example project.

**B.** [Blank project](create_with_mkdocs.md#b-blank-project): If you are confident with structuring your git repository on your own, continue with a blank project.


## A. Import project

Create a new project by clicking on the upper right button **New project** and select the **Import project** option. This will show several importing sources. Choose the **Repository by URL** option. It will open a form to fill in the information:

* **Git repository URL**: Enter: `https://gitlab.cern.ch/authoring/documentation/mkdocs-container-example.git`. This will help you start with a very simple example MkDocs structure with minimal content.
* **Mirror repository**: Leave unchecked.
* **Username (optional)**: Leave empty.
* **Password (optional)**: Leave empty.
* **Project name**: Enter a descriptive name for this git repository. For example "My new GitLab Pages site". This will help you and other users remember what this git repository is for.
* **Project URL**: Select one of the available options, essentially the *Groups* and *Users* you have access to on GitLab. There might already exist a group for your service, otherwise, we suggest you [create one](https://gitlab.cern.ch/groups/new){target=_blank} first and then select it here. This, together with the **Project slug** will form the URL of your git repository.
* **Project slug**: Enter the slug of your git repository. This, together with the **Project URL**, will form the URL of your git repository. For example, if you selected `service` as the **Project URL** and `docs` as the **Project slug**, your git repository will be available at `https://gitlab.cern.ch/service/docs`. Please, we encourage you to use `docs` as the `Project slug` for static documentation sites, as you can see in the following example.
* **Project description (optional)**: Enter a description of your git repository. For example "This is my new GitLab Pages site". This will help you and other users remember what this git repository is for.
* **Visibility Level**: Select the visibility level of your git repository. This only affects who has access to the Markdown sources for your documentation.
    * "Private" means you have to explicitly give read access to other users.
    * "Internal" means all CERN users have read access.
    * "Public" means that everyone on the Internet has read access.

!!! warning
    The name and the URL of your git repository are completely independent of your site name and URL.

![form](/images/gitlab-import-repo.png)


Once you filled in the form, click on **Create project** and a repository will be created with an exemplary mkdocs configuration.


### Build artifacts for an imported project

The last step if you opt for importing a project is to build and push the artifacts needed to deploy the site. Since [a special template](https://gitlab.cern.ch/authoring/documentation/mkdocs-ci/-/blob/master/mkdocs-gitlab-pages.gitlab-ci.yml){target=_blank} **is already included** in your `.gitlab-ci.yml`, **the only mandatory thing that you have to do** is to run the pipeline to build the static assets for the first time. Go to CI/CD section in the left panel of your repository and click on the Pipelines option. Once there, click on the upper right button **Run Pipeline**. A new screen will appear asking you to select the branch and set variables (by default, the pipeline should be run for the [repository's default branch](https://docs.gitlab.com/ee/user/project/repository/branches/default.html#change-the-default-branch-name-for-a-project){target=_blank}). Ignore it and click on **Run Pipeline** button.

![pipelines_vars](/images/gitlab-pipelines-vars.png)


Wait until the job ends successfully. You will see a new job in the CI/CD -> Pipelines page:

![pipelines](/images/gitlab-pipelines-jobs-success.png)


!!! info
    For more context: in order to have the changes automatically deployed we need to first build the MkDocs documentation artifacts before `GitLab` will automatically push them to a safe storage (pushing is fully handled by GitLab so you don't need to worry about it). Thanks to the template included in `.gitlab-ci.yml` configuration which you should use, static content will be built and pushed every time a new commit is pushed to the [repository's default branch](https://docs.gitlab.com/ee/user/project/repository/branches/default.html#change-the-default-branch-name-for-a-project){target=_blank} (or if a branch is merged to the [repository's default branch](https://docs.gitlab.com/ee/user/project/repository/branches/default.html#change-the-default-branch-name-for-a-project){target=_blank}). With this new setup, all your future changes will be automatically deployed once the GitLab CI job rebuilds your static site with your latest changes.

!!! warning
    This procedure is only needed the first time, when you create the project. With this new setup, all changes are automatically deployed once the GitLab CI job rebuilds your static site from now on.


## B. Blank project

Create a new project by clicking on the upper right button **New project** and select the **Create blank project** option. This will open a form to fill in the information:

* **Project name**: Enter a descriptive name for this git repository. For example "My new GitLab Pages site". This will help you and other users remember what this git repository is for.
* **Project URL**: Select one of the available options, essentially the *Groups* and *Users* you have access to on GitLab. There might already exist a group for your service, otherwise, we suggest you [create one](https://gitlab.cern.ch/groups/new){target=_blank} first and then select it here. This, together with the **Project slug** will form the URL of your git repository.
* **Project slug**: Enter the slug of your git repository. This, together with the **Project URL**, will form the URL of your git repository. For example, if you selected `service` as the **Project URL** and `docs` as the **Project slug**, your git repository will be available at `https://gitlab.cern.ch/service/docs`. Please, we encourage you to use `docs` as the `Project slug` for static documentation sites, as you can see in the previous example.
* **Project description (optional)**: Enter a description of your git repository. For example "This is my new GitLab Pages site". This will help you and other users remember what this git repository is for.
* **Visibility Level**: Select the visibility level of your git repository. This only affects who has access to the Markdown sources for your documentation.
    * "Private" means you have to explicitly give read access to other users.
    * "Internal" means all CERN users have read access.
    * "Public" means that everyone on the Internet has read access.
* **Project Configuration**: You can uncheck the _Initialize repository with a README_ checkbox.

![form](/images/gitlab-create-blank-repo.png)


Once you have filled in the form, click on **Create project** and an empty repository will be created.


### Build artifacts for a blank project

The last step if you opt for creating a blank project is to build and push the artifacts needed to deploy the site. If you want an easy and automatic way to do it we recommend you to [include a special template](https://gitlab.cern.ch/authoring/documentation/mkdocs-ci/-/blob/master/mkdocs-gitlab-pages.gitlab-ci.yml){target=_blank} on top of your `.gitlab-ci.yml` by copying there the following piece of code:
    ```
    include:
        - project: 'authoring/documentation/mkdocs-ci'
          file: 'mkdocs-gitlab-pages.gitlab-ci.yml'
    ```

!!! info
    If you need some help on how to create a `.gitlab-ci.yml` file please, follow the following steps:
    
    1. Click on `New file` button.
    ![GitLab CI step 1](/images/gitlab-ci-file-step-1.png)

    2. Click on the `New file` button again. A modal will be opened, click on `.gitlab-ci.yml`.
    ![GitLab CI step 2](/images/gitlab-ci-file-step-2.png)
    
    3. Add the template mentioned above with the same exact indentation and click on `Create commit...` button.
    ![GitLab CI step 3](/images/gitlab-ci-file-step-3.png)
    
    4. Click on `Commit` button.
    ![GitLab CI step 4](/images/gitlab-ci-file-step-4.png)


This will build and push to a safe storage the static assets every time a new commit is pushed to the [repository's default branch](https://docs.gitlab.com/ee/user/project/repository/branches/default.html#change-the-default-branch-name-for-a-project){target=_blank} (or a branch is merged to [repository's default branch](https://docs.gitlab.com/ee/user/project/repository/branches/default.html#change-the-default-branch-name-for-a-project){target=_blank}). You can modify it to your liking.

!!! info
    For more context: in order to have the changes automatically deployed we need to first build the MkDocs documentation artifacts before `GitLab` will automatically push them to a safe storage (pushing is fully handled by GitLab so you don't need to worry about it). Thanks to the previous `.gitlab-ci.yml` configuration which you should use, static content will be built and pushed every time a new commit is pushed to the [repository's default branch](https://docs.gitlab.com/ee/user/project/repository/branches/default.html#change-the-default-branch-name-for-a-project){target=_blank} (or if a branch is merged to [repository's default branch](https://docs.gitlab.com/ee/user/project/repository/branches/default.html#change-the-default-branch-name-for-a-project){target=_blank}). With this new setup, all your future changes will be automatically deployed once the GitLab CI job rebuilds your static site with your latest changes.


!!! warning
    With this new setup all changes are automatically deployed once the GitLab CI job rebuilds your static site.


## Customise your mkdocs.yml configuration file

Once you are done with the creation of the repository and the required files, you probably want to customise some features of your site, by customising `mkdocs.yml` file. You can find further information about how to do it in [this section](/advanced/customise_mkdocs_file/){target=_blank}.

## Next step 

Good! your repository is configured. Now you can go to the next step to [create a site in Web Services](/gitlabpagessite/create_site/create_webeos_project/).
