# How to create files in GitLab

Great! Your site is configured. Now it's time to populate it with actual content. If you want to use GitLab editor to write it, you can follow [this documentation](https://gitlab.cern.ch/help/user/project/repository/web_editor.md#gitlab-web-editor){target=_blank}. You can also use integrated [Web IDE](https://docs.gitlab.com/ee/user/project/web_ide/) for more complex changes.
